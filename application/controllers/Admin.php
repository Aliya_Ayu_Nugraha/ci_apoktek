<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin');
	}

	public function index()
	{
		if ($this->session->userdata('login')!=TRUE) {
			redirect('admin/login','refresh');
		}

		$data ['konten']="konten";
		$this->load->view('template', $data);
	}

	public function login()
	{
		if ($this->session->userdata('login')==TRUE) {
			redirect('admin','refresh');
		}

		else{
			$this->load->view('login');
		}
	}

	public function proses_login()
	{
		if($this->input->post('login')){
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$this->load->model('m_admin');
				if ($this->m_admin->get_login()->num_rows()>0) {
					$data=$this->m_admin->get_login()->row();
					$array=array(
						'login'=> TRUE,
						'nama_admin' => $data->nama_admin,
						'username'=> $data->username,
						'password'=> $data->password,
						'level' => $data->level
					);

					$this->session->set_userdata( $array );
					redirect('admin','refresh');
				}else {
				$this->session->set_flashdata('pesan', 'username dan password salah');
				redirect('admin/login','refresh');
				} 
			}else{
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('admin/login','refresh');
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('admin/login','refresh');
	}

	public function tampiladmin()
	{
		$data['tampil']=$this->m_admin->tampiladmin();
		$data['konten']= "tampil_admin";
		$this->load->view('template', $data);
	}

	public function hapus($id_admin='')
	{
		if ($this->m_admin->hapus_admin($id_admin)) {
			redirect('admin/tampiladmin','refresh');
		}
	}
	public function tambah_admin()
	{
		$data['konten'] = "tambah_admin";
		$this->load->view('template', $data);
	}
	public function proses_tambah()
	{
		if ($this->input->post('tambah')){
			$this->form_validation->set_rules('nama_admin', 'nama admin', 'trim|required');
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');
			$this->form_validation->set_rules('level', 'level', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				if ($this->m_admin->simpan_admin() == TRUE){
					redirect('admin/tampiladmin','refresh');
				}else{
					$this->session->set_flashdata('pesan', 'Gagal Menyimpan');
					redirect('kategori/tambah_admin','refresh');
				}
			} else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('kategori/tambah_kategori','refresh');
			}
		}
	}

	public function tampil_edit_admin($id_admin='')
	{
		$where=array(
			'id_admin'=>$id_admin
		);
		$data ['adminlama'] = $this->m_admin->ambil_admin($where,'admin')->result();
		$data ['konten'] ="ubah_admin";
		$this->load->view('template', $data);
	}
	public function proses_update()
	{
		if ($this->input->post('ubah')) {
		if ($this->m_admin->simpan_admin_baru() == TRUE) {
			redirect('admin/tampiladmin','refresh');
		}else{
			redirect('admin/tampil_edit_admin','refresh');
		}
	}

}
}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */