<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {


	public function __construct()
	{
		parent:: __construct();

		$this->load->model('m_kategori','kat');
	}
	public function index()
	{
		$data ['tampil_kat'] = $this->kat->tampil_kat();
		$data ['judul'] = "Kategori";
		$data ['konten'] = "v_kategori";
		$this->load->view('template', $data);
		
	}

	//halaman tambah kategori
	public function tambah_kategori()
	{
		$data ['konten'] = "tambah_kategori";
		$this->load->view('template', $data);
	}

	public function proses_kategori()
	{
		
		if ($this->input->post('tambah_kategori')){
			$this->form_validation->set_rules('id_kategori', 'id kategori', 'trim|required');
			$this->form_validation->set_rules('nama_kategori', 'nama kategori', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				if ($this->kat->simpan_kategori() == TRUE){
					redirect('kategori','refresh');
				}else{
					$this->session->set_flashdata('pesan', 'Gagal Menyimpan');
					redirect('kategori/tambah_kategori','refresh');
				}
			} else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('kategori/tambah_kategori','refresh');
			}
		}
	}

	public function tampil_edit_kategori($id_kategori='')
	{
		$where=array(
			'id_kategori'=>$id_kategori
		);
		$data ['kategori'] = $this->kat->ambil_kategori($where,'kategori')->result();
		$data ['konten'] = "edit_kategori";
		$this->load->view('template', $data);
	}

	public function update()
	{
		if ($this->input->post('edit')) {

		if ($this->kat->simpan_kategori_baru() == True) {
			redirect('kategori','refresh');
		}else{
			redirect('kategori/tampil_edit_kategori','refresh');
		}
	}
		
	}

	public function hapus($id_kategori='')
	{
		if ($this->kat->hapus_kategori($id_kategori)) {
			redirect('kategori','refresh');
		}
	}

}

/* End of file Kategori.php */
/* Location: ./application/controllers/Kategori.php */