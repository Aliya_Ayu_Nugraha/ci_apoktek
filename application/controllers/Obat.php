<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('m_obat');
	}

	public function index()
	{
		$data['tampil_obat']=$this->m_obat->tampil_obat();
		$data['konten']="v_obat";
		$this->load->view('template', $data);
		
	}

	public function tambah_obat()
	{
		$data ['tambah_obat'] = $this->m_obat->tampil_kategori();
		$data ['konten'] = "tambah_obat";
		$this->load->view('template', $data);
	}

	public function proses_obat()
	{
		if ($this->input->post('tambah_obat')){
			$this->form_validation->set_rules('nama_obat', 'Nama Obat', 'trim|required');
			$this->form_validation->set_rules('harga_obat', 'Harga Obat', 'trim|required');
			$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				if ($this->m_obat->simpan_obat()) {
					redirect('obat','refresh');
				}
			} else {
				$this->session->set_flashdata('pesan','Gagal Menambah');
				redirect('obat/tambah_obat','refresh');
			}
		}
		else{
			$this->session->set_flashdata('pesan', validation_errors());
			redirect('obat/tambah_obat','refresh');
		}
	}

	public function tampil_edit_obat($id_obat='')
	{
		$where=array(
			'id_obat'=>$id_obat
		);
		$data ['obat'] = $this->m_obat->tampil_obat_lama($where,'obat')->result();
		$data['tampil_kategori']=$this->m_obat->tampil_kategori();
		$data ['konten'] ="edit_obat";
		$this->load->view('template', $data);
	}

	public function update()
	{
		if($this->input->post('edit')){
			if($this->m_obat->simpan_edit_obat() == TRUE){
				redirect('obat','refresh');
			}else{
				redirect('obat/tampil_edit_obat','refresh');
			}
		}
	}
	public function hapus($id_obat='')
	{
		if ($this->m_obat->hapus($id_obat)) {
			redirect('obat','refresh');
		}
	}

}

/* End of file Obat.php */
/* Location: ./application/controllers/Obat.php */