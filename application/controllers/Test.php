<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	public function __construct()
		{
			parent::__construct();
			$this->load->helper('download');


			if($this->session->userdata('login')!=TRUE){
			redirect('Login','refresh');
			}
	
			$this->load->model('M_project','sit');
		}

	
		public function download()
		{
			if (!empty($id_project)) {
				$this->load->helper('download');
				$fileInfo= $this->sit->getRows(array('id_project'=>$id_project));
				$file = 'assets/'.$fileInfo['caf'];
				force_download($file, $fileInfo);
			}
		}	


	public function index()
	{
		$data['detail_p']=$this->sit->tampil_tambah();
		$data['site']=$this->sit->data_site();
		$data['konten']="V_project";
		$data['judul']="Permit Akses Project";
			$data['jumlah_project']=$this->sit->jumlah_project();
		$data['jumlah_nonproject']=$this->sit->jumlah_nonproject();
		$this->load->view('template', $data);
	}

	public function user()
	{
		$data['konten']="V_tambah_user";
		$data['judul']="Permit Akses Project";
			$data['jumlah_project']=$this->sit->jumlah_project();
		$data['jumlah_nonproject']=$this->sit->jumlah_nonproject();
		$this->load->view('template', $data);
	}

	public function view()
	{
		$data['detail_p']=$this->sit->tampil_tambah();
		$data['site']=$this->sit->data_site();
		$data['konten']="V_tambah";
		$data['jumlah_project']=$this->sit->jumlah_project();
		$data['jumlah_nonproject']=$this->sit->jumlah_nonproject();
		$data['judul']="Permit Akses Project";
		$this->load->view('template', $data);
	}

		public function approve()
	{
		$data['detail_p']=$this->sit->tampil_tambahdone();
		$data['detail_np']=$this->sit->tampil_tambahdonee();
		$data['konten']="V_datadone";
		$data['judul']="Permit Akses Project";
			$data['jumlah_project']=$this->sit->jumlah_project();
		$data['jumlah_nonproject']=$this->sit->jumlah_nonproject();
		$this->load->view('template', $data);
	}

	public function edit_approve($id)
	{
		$data=$this->sit->detail_a($id);
		echo json_encode($data);
	}

	public function ubah()
	{
    if ($this->input->post('edit')) {
      if ($this->sit->edit_approve()) {
        $this->session->set_flashdata('pesan', 'Sukses Edit Approve');
        redirect('Project/view', 'refresh');
      }else {
        $this->session->set_flashdata('pesan', 'Gagal Edit Aprrove');
        redirect('Project/view', 'refresh');
      }
    }
}

public function edit_buku($id)
	{
		$data=$this->buku->detail_ga($id);
		echo json_encode($data);
	}

public function update_gambar()
	{
		if ($this->input->post('edit'))
		{
			if ($_FILES['file_ktp']['name']=="") {
				if ($this->sit->buku_update_no_foto()) {
					$this->session->set_flashdata('pesan', 'Sukses Update');
					redirect('Project/view', 'refresh');
				} else {
					$this->session->set_flashdata('pesan', 'Gagal Update');
					redirect('Project/view', 'refresh');
				}
			} else {
				$config['upload_path'] = './assets/images/';
				$config['allowed_types'] = 'gif|jpg|png|doc|pdf|xls';
				$config['max_size']  = '10000';
				$config['max_width']  = '10240';
				$config['max_height']  = '7680';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('file_ktp')){
					$this->session->set_flashdata('pesan', "Gagal upload");
					redirect('Project/view', 'refresh');
				}
				else{
					if ($this->sit->buku_update_dengan_foto($this->upload->data('file_name'))) {
						$this->session->set_flashdata('pesan', 'Sukses Update');
						redirect('Project/view', 'refresh');
					} else {
						$this->session->set_flashdata('pesan', 'Gagal Update');
						redirect('Project/view', 'refresh');
					}

				}
			}

		}
	}



	public function nomor()
	{
		$data['konten']="nomor";
		$data['judul']="Data Surat";
			$data['jumlah_project']=$this->sit->jumlah_project();
		$data['jumlah_nonproject']=$this->sit->jumlah_nonproject();
		$this->load->view('Template', $data);
	}

	public function simpan_nomor()
	{
		if ($this->sit->nomor()) {
			$this->session->set_flashdata('pesan', 'Gagal Tambah Data');
			redirect('Project/nomor','refresh');
		} else {
			$this->session->set_flashdata('pesan', 'Sukses Tambah Data');
			redirect('Project/nomor','refresh');
		}
	}

	public function detail_g($id)
  {
    $this->load->model('M_project','sit', TRUE);
    $data['judul']="Ubah Data KTP";
    $data['detail_g']=$this->sit->detail_ga($id);
   	$data['jumlah_project']=$this->sit->jumlah_project();
	$data['jumlah_nonproject']=$this->sit->jumlah_nonproject();
    $data['konten']="V_Detaily.php";
    $this->load->view('template', $data);
  }

	public function detail_p($id_project)
  {
    $this->load->model('M_project','sit', TRUE);
    $data['judul']="Detail Data";
    $data['detail_p']=$this->sit->detail_p($id_project);
    $data['detail_s']=$this->sit->detail_s();
   	$data['jumlah_project']=$this->sit->jumlah_project();
		$data['jumlah_nonproject']=$this->sit->jumlah_nonproject();
    $data['konten']="V_DetailNp.php";
    $this->load->view('V_DetailNp', $data);
  }

  	public function hapus_data($id="")
	{
		if ($this->sit->hapus_data($id)) {
			$this->session->set_flashdata('pesan', 'Sukses Hapus Data');
		} else {
			$this->session->set_flashdata('pesan', 'Gagal Hapus Data');
		}
		redirect('Project/view','refresh');
	}
public function simpan_g()
	{
		$this->form_validation->set_rules('id_project', 'id_project', 'trim|required');

	if ($this->form_validation->run() == TRUE) {
			$config['upload_path'] = './assets/';
			$config['allowed_types'] = 'pdf|doc|xls|png|jpg|jpeg|gif';

	if ($_FILES['file_ktp']['name']!="") {
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('file_ktp')){
					$this->session->set_flashdata('pesan', $this->upload->display_errors());
				}
				else{
					if ($this->sit->simpan_gambar($this->upload->data('file_name'))) {
						$this->session->set_flashdata('pesan','sukses menambah');
					} else {
						$this->session->set_flashdata('pesan', 'gagal menambah');
					}
				redirect('Project/view','refresh');	
				}
			}
			 else {
				if ($this->sit->simpan_gambar('')) {
					$this->session->set_flashdata('pesan', ' sukses menambah');
				} else {
					$this->session->set_flashdata('pesan','gagal menambah');
				}
				redirect('Project/view','refresh');	
			}
		 }
		 else {
			$this->session->set_flashdata('pesan', validation_errors());
			redirect('Project/view','refresh');	
	}
}



	

	public function simpan()
	{
		if ($this->input->post('submit')) {
		$this->form_validation->set_rules('SOW', 'sow', 'trim|required');
		$this->form_validation->set_rules('id_site', 'id_site', 'trim|required');
		$this->form_validation->set_rules('approve', 'approve', 'trim');
		$this->form_validation->set_rules('nama1', 'nama1', 'trim');
		$this->form_validation->set_rules('nomor', 'nomor', 'trim');
		$this->form_validation->set_rules('email', 'email', 'trim');
		$this->form_validation->set_rules('pelaksanaan', 'pelaksanaan', 'trim');
		$this->form_validation->set_rules('durasi', 'durasi', 'trim|required');
		$this->form_validation->set_rules('nama2', 'nama2', 'trim');
		$this->form_validation->set_rules('nama3', 'nama3', 'trim');
		$this->form_validation->set_rules('nama4', 'nama4', 'trim');
		$this->form_validation->set_rules('nama5', 'nama5', 'trim');
		$this->form_validation->set_rules('nama6', 'nama6', 'trim');
		$this->form_validation->set_rules('nama7', 'nama7', 'trim');
		$this->form_validation->set_rules('nama8', 'nama8', 'trim');
		$this->form_validation->set_rules('telp1', 'telp1', 'trim');
		$this->form_validation->set_rules('telp2', 'telp2', 'trim');
		$this->form_validation->set_rules('telp3', 'telp3', 'trim');
		$this->form_validation->set_rules('telp4', 'telp4', 'trim');
		$this->form_validation->set_rules('telp5', 'telp5', 'trim');
		$this->form_validation->set_rules('telp6', 'telp6', 'trim');
		$this->form_validation->set_rules('telp7', 'telp7', 'trim');
		$this->form_validation->set_rules('telp8', 'telp8', 'trim');
		$this->form_validation->set_rules('tim1', 'tim1', 'trim');
		$this->form_validation->set_rules('tim2', 'tim2', 'trim');
		$this->form_validation->set_rules('tim3', 'tim3', 'trim');
		$this->form_validation->set_rules('tim4', 'tim4', 'trim');
		$this->form_validation->set_rules('tim5', 'tim5', 'trim');
		$this->form_validation->set_rules('tim6', 'tim6', 'trim');
		$this->form_validation->set_rules('tim7', 'tim7', 'trim');
		$this->form_validation->set_rules('tim8', 'tim8', 'trim');
		$this->form_validation->set_rules('caf', 'caf', 'trim');
		$this->form_validation->set_rules('file_ktp', 'file_ktp', 'trim');

		if ($this->form_validation->run() == TRUE) {
				if ($this->sit->simpan_caf() == TRUE){
						redirect('Project/view','refresh');
				}else{
					$this->session->set_flashdata('pesan', 'Gagal Menyimpan');
					redirect('Project/view','refresh');
				}
			} else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('Project/view','refresh');
			}
		}


	
	}
}
   public function cari(){
	 $data['cariberdasarkan']=$this->input->post("cariberdasarkan");
    $data['yangdicari']=$this->input->post("yangdicari");
      $data["detail_g"] = $this->sit->cari( $data['cariberdasarkan'],$data['yangdicari'])->result();
    $data["jumlah"]=count($data["detail_g"]);
      $data['judul']="Data Hasil";
      $data['konten']="v_Detaily";
      	$data['jumlah_project']=$this->sit->jumlah_project();
		$data['jumlah_nonproject']=$this->sit->jumlah_nonproject();
    $this->load->view('template', $data);
  }

}




/* End of file Project.php */
/* Location: ./application/controllers/Project.php */