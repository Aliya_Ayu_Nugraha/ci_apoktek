<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_obat');
		$this->load->model('m_transaksi');
	}

	public function index()
	{

		$data['tampil_obat'] = $this->m_obat->tampil_obat();
		$data['konten']="v_transaksi";
		$this->load->view('template', $data);
	}

}

/* End of file v_transaksi.php */
/* Location: ./application/models/v_transaksi.php */