<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Project extends CI_Model {

	public function tampil_site()
	{
		$tm_site=$this->db
					->join('site','site.id_site=project.id_site')
					  ->get('project')
					  ->result();
		return $tm_site;
	}

	   public function tampil_gambar($file_name)
  	{
   		$tm=$this->db->where('file_name', $file_name)
        ->get('gambar_multi')
        ->row();
    return $tm;
 	 }


	public function cari($berdasarkan,$yangdicari){

    $this->db->from('gambar_multi');
    $this->db->like($berdasarkan,$yangdicari);
    return $this->db->get();

  }

public function jumlah_project(){
		 $ja=$this->db->where('approve', 'notdone')
        ->get('project')
        ->num_rows();
    return $ja;
  }

  public function jumlah_nonproject(){
		 $ja=$this->db->where('approve', 'notdone')
        ->get('nonproject')
        ->num_rows();
    return $ja;
  }

public function getUser()
	{
		return $this->db->get('user')->result();
	}


	public function tampil_tambah()
	{
		$tm_tambah=$this->db
					->where('approve','notdone')
					  ->get('project')->result();
		return $tm_tambah;
	}

		public function tampil_tambahdone()
	{
		$tm_tambah=$this->db
					->where('approve','done')
					  ->get('project')->result();
		return $tm_tambah;
	}

	public function detail_a($a)
	{
		return $this->db->where('id_project', $a)
						->get('project')
						->row();
	}

	public function edit_approve()
	{
		$object = array(
			'approve' => $this->input->post('approve'),
			'username' => $this->input->post('username'),
			'PT' => $this->input->post('PT') );

		return $this->db->where('id_project', $this->input->post('id_approve_lama'))->update('project',$object);
	}



	public function nomor()
	{
		$object = array(
			'id_nomor' => $this->input->post('id_nomor'),
			'alamat' => $this->input->post('alamat'),
			'kepada' => $this->input->post('kepada'));

			$this->db->insert('nomor', $object);
	}

	public function detail($a)
	{
		$tm_site=$this->db
					  ->join('site','site.id_site=project.id_site')
					  ->where('id_project',$a)
					  ->get('project')
					  ->row();
		return $tm_site;
	}
	public function data_site()
	{
		return $this->db->get('site')->result();
	}


	public function simpan_caf()
	{
		$sow=$this->input->post('SOW');
		$id_site=$this->input->post('id_site');
		$approve=$this->input->post('approve');
		$pelaksanaan=$this->input->post('pelaksanaan');
		$durasi=$this->input->post('durasi');
		$nama1=$this->input->post('nama1');
		$nama2=$this->input->post('nama2');
		$nama3=$this->input->post('nama3');
		$nama4=$this->input->post('nama4');
		$nama5=$this->input->post('nama5');
		$nama6=$this->input->post('nama6');
		$nama7=$this->input->post('nama7');
		$nama8=$this->input->post('nama8');
		$telp1=$this->input->post('telp1');
		$telp2=$this->input->post('telp2');
		$telp3=$this->input->post('telp3');
		$telp4=$this->input->post('telp4');
		$telp5=$this->input->post('telp5');
		$telp6=$this->input->post('telp6');
		$telp7=$this->input->post('telp7');
		$telp8=$this->input->post('telp8');
		$tim1=$this->input->post('tim1');
		$tim2=$this->input->post('tim2');
		$tim3=$this->input->post('tim3');
		$tim4=$this->input->post('tim4');
		$tim5=$this->input->post('tim5');
		$tim6=$this->input->post('tim6');
		$tim7=$this->input->post('tim7');
		$tim8=$this->input->post('tim8');
		$ktp1=$this->input->post('ktp1');
		$ktp2=$this->input->post('ktp2');
		$ktp3=$this->input->post('ktp3');
		$ktp4=$this->input->post('ktp4');
		$ktp5=$this->input->post('ktp5');
		$ktp6=$this->input->post('ktp6');
		$ktp7=$this->input->post('ktp7');
		$ktp8=$this->input->post('ktp8');
		$subcont=$this->input->post('subcont');
		$nomor=$this->input->post('nomor');
		$file_ktp=$this->input->post('file_ktp');
		$caf=$this->input->post('caf');

		$object=array(
			'sow'=>$sow,
			'id_site'=>$id_site,
			'caf'=>$caf,
			'file_ktp'=>$file_ktp,
			'subcont'=>$subcont,
			'approve'=>$approve,
			'nama1'=>$nama1,
			'nama2'=>$nama2,
			'nama3'=>$nama3,
			'nama4'=>$nama4,
			'nama5'=>$nama5,
			'nama6'=>$nama6,
			'nama7'=>$nama7,
			'nama8'=>$nama8,
			'telp1'=>$telp1,
			'telp2'=>$telp2,
			'telp3'=>$telp3,
			'telp4'=>$telp4,
			'telp5'=>$telp5,
			'telp6'=>$telp6,
			'telp7'=>$telp7,
			'telp8'=>$telp8,
			'tim1'=>$tim1,
			'tim2'=>$tim2,
			'tim3'=>$tim3,
			'tim4'=>$tim4,
			'tim5'=>$tim5,
			'tim6'=>$tim6,
			'tim7'=>$tim7,
			'tim8'=>$tim8,
			'ktp1'=>$ktp1,
			'ktp2'=>$ktp2,
			'ktp3'=>$ktp3,
			'ktp4'=>$ktp4,
			'ktp5'=>$ktp5,
			'ktp6'=>$ktp6,
			'ktp7'=>$ktp7,
			'ktp8'=>$ktp8,
			'nomor'=>$nomor,
			'pelaksanaan'=>$pelaksanaan,
			'durasi'=>$durasi);

			$this->db->insert('project', $object);
		
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else{
			return false;
		}
	}

public function detail_np($id_nonproject)
{
    $detail_np=$this->db->where('id_nonproject', $id_nonproject)
        ->get('nonproject')
        ->row();
    return $detail_np;
  }


	public function detail_p($id_project)
{
    $detail_p=$this->db->where('id_project', $id_project)
        ->get('project')
        ->row();
    return $detail_p;
  }
  public function tampil_tambahdonee()
	{
		$tm_tambah=$this->db
					->where('approve','DONE')
					  ->get('nonproject')->result();
		return $tm_tambah;
	}

	public function tampil_tambahh()
	{
		$tm_tambah=$this->db
					->where('approve','NOTDONE')
					  ->get('nonproject')->result();
		return $tm_tambah;
	}

public function buku_update_no_foto()
			{
				$object=array(
					'file_ktp'=>$this->input->post('file_ktp')
				);
				return $this->db->where('id_project',$this->input->post('id_project'))->update('project',$object);
			}

			public function buku_update_dengan_foto($file_ktp="")
			{
				$object=array(
					'file_ktp'=>$file_ktp);
				return $this->db->where('id_project',$this->input->post('id_project'))->update('project',$object);
			}



	public function detail_ga($a)
			{
				$tm_buku=	$this->db
									->where('id_project',$a)
									->get('project')
									->row();
				return $tm_buku;

			}

  public function detail_s()
{
    $detail_site=$this->db
        ->get('site')
        ->row();
    return $detail_site;
  }

  public function detail_download($caf)
  {
    $detail_download=$this->db->where('caf', $caf)
        ->get('project')
        ->row();
    return $detail_download;
  }

  	public function hapus_data($id='')
	{
		return $this->db->where('id_project',$id)->delete('project');
	}

	public function getRows($params=array())
		{
			$this->db->select('*');
        	$this->db->from('project');
        	
        	if(array_key_exists('id_project',$params) && !empty($params['id_project'])){
            	$this->db->where('id_project',$params['id_project']);
            	//get records
            	$query = $this->db->get();
            	$result = ($query->num_rows() > 0)?$query->row_array():FALSE;

        	} else {
            	//get records
            	$query = $this->db->get();
            	$result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        	}
        		//return fetched data
        		return $result;
		}	

		public function simpan_gambar($file_name)
	{
		$id_project=$this->input->post('id_project');

		$object=array(
			'id_project'=>$id_project,
			'file_name'=>$file_name,);

			$this->db->insert('gambar_multi', $object);
		
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else{
			return false;
		}
	}	


}


/* End of file M_Project.php */
/* Location: ./application/models/M_Project.php */