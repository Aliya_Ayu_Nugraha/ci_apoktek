<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

	public function get_login()
	{
		return $this->db
			->where('username',$this->input->post('username'))
			->where('password',$this->input->post('password'))
			->get('admin'); 
	}

	public function tampiladmin()
	{
		$tm_admin =	$this->db->get('admin')->result();
		return $tm_admin;
	}

	public function hapus_admin($id_admin='')
	{
		return $this->db->where('id_admin',$id_admin)->delete('admin');
	}

	public function simpan_admin()
	{
			$nama_admin = $this->input->post('nama_admin');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$level = $this->input->post('level');

			$object=array(
					'nama_admin'=>$nama_admin,
					'username'=>$username,
					'password'=>$password,
					'level'=>$level
			);
			return $this->db->insert('admin',$object);
	}

	public function ambil_admin($where, $table)
	{
			return $this->db->get_where($table,$where);
	}

	public function simpan_admin_baru()
	{
			$id_admin = $this->input->post('id_admin');
			$nama_admin = $this->input->post('nama_admin');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$level = $this->input->post('level');

			$object=array(
				'id_admin' => $id_admin,
				'nama_admin' => $nama_admin,
				'username' => $username,
				'password' => $password,
				'level' => $level,
			);

			$where=array(
				'id_admin' =>$id_admin
			);

			return $this->db->where($where)->update('admin',$object);

	}

}

/* End of file M_admin.php */
/* Location: ./application/models/M_admin.php */