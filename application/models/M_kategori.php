<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kategori extends CI_Model {

		public function tampil_kat()
		{
			$tm_kategori =$this->db
			->get('kategori')->result();
			return $tm_kategori;
		}

		public function detil_kategori($a)
		{
			return $this->db->where('id_kategori',$a)
			->get('kategori')
			->row();
		}
		public function simpan_kategori()
		{
			$id_kategori = $this->input->post('id_kategori');
			$nama_kategori = $this->input->post('nama_kategori');

			$object=array(
					'id_kategori'=>$id_kategori,
					'nama_kategori'=>$nama_kategori
			);
			return $this->db->insert('kategori',$object);
		}

		
		public function hapus_kategori($id_kategori='')
		{
			return $this->db->where('id_kategori',$id_kategori)->delete('kategori');
		}

		public function ambil_kategori($where, $table)
		{
			return $this->db->get_where($table,$where);
		}

		public function simpan_kategori_baru()
		{
			$id_kategori = $this->input->post('id_kategori');
			$nama_kategori = $this->input->post('nama_kategori');

			$object=array(
				'id_kategori' => $id_kategori,
				'nama_kategori' => $nama_kategori
			);

			$where=array(
				'id_kategori' =>$id_kategori
			);

			return $this->db->where($where)->update('kategori',$object);
		}
	

}

/* End of file M_kategori.php */
/* Location: ./application/models/M_kategori.php */