<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_obat extends CI_Model {

	public function tampil_kategori()
	{
		return $this->db->get('kategori')->result();
	}
	public function tampil_obat()
	{
		$tm_obat=$this->db->join('kategori','kategori.id_kategori=obat.id_kategori')->get('obat')->result();
		return $tm_obat;
	}

	public function simpan_obat()
	{
		$nama_obat = $this->input->post('nama_obat');
		$harga_obat = $this->input->post('harga_obat');
		$kategori = $this->input->post('kategori'); 

		$object = array(
			'nama_obat'  => $nama_obat,
			'harga_obat' => $harga_obat,
			'id_kategori' => $kategori
		);

		return $this->db->insert('obat', $object);
	}
	public function tampil_obat_lama($where,$table)
	{
		return $this->db->get_where($table,$where);
	}
	public function simpan_edit_obat()
	{
		$id_obat = $this->input->post('id_obat');
		$nama_obat = $this->input->post('nama_obat');
		$harga_obat = $this->input->post('harga_obat');
		$id_kategori = $this->input->post('kategori');

		$object= array(
			'id_obat' => $id_obat,
			'nama_obat' => $nama_obat,
			'harga_obat' => $harga_obat,
			'id_kategori' => $id_kategori
		);

		$lokasi = array(
			'id_obat'=> $id_obat
		);

		return $this->db->where($lokasi)->update('obat',$object);
	}

	public function hapus($id_obat='')
	{
		return $this->db->where('id_obat',$id_obat)->delete('obat');
	}
	

}

/* End of file M_obat.php */
/* Location: ./application/models/M_obat.php */