<div class="col-md-6" style="margin-top: 80px">
  <?php foreach ($obat as $obt) {?>
<form action="<?=base_url('index.php/obat/update')?>" method="post">
  <input type="hidden" name="id_obat" value="<?= $obt->id_obat?>">
    <div class="form-group">
      <label >Nama Obat</label>
      <input name="nama_obat" type="text"  class="form-control" placeholder="Nama Obat" value="<?= $obt->nama_obat?>">
    </div>
    <div class="form-group">
      <label>Harga Obat</label>
      <input name="harga_obat" type="text" class="form-control" placeholder="Harga Obat" value="<?= $obt->harga_obat?>">
    </div>
    <div class="form-group">
      <label>Pilih Kategori</label>
      <select name="kategori" class="form-control">
        <option><?= $obt->id_kategori?></option>
        <?php foreach ($tampil_kategori as $kat) {?>
        <option><?=$kat->id_kategori?></option>
        <?php } ?>
      </select>
    </div>
    <input name="edit" type="submit" class="btn btn-primary" value="edit"></input>
</form>
<?php } ?>
</div>