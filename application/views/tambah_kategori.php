
<div class="col-md-6 col-md-offset-3" style="margin-top: 100px">
<form class="form-horizontal" action="<?=base_url('index.php/kategori/proses_kategori')?>" method="post">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">ID Kategori</label>
    <div class="col-sm-10">
      <input name="id_kategori" type="text" class="form-control"  placeholder="id kategori">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Nama Kategori</label>
    <div class="col-sm-10">
      <input name="nama_kategori" type="text" class="form-control" placeholder="Nama Kategori">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <input name="tambah_kategori" type="submit" class="btn btn-success" value="Tambah"></input>
    </div>
  </div>
</form>
</div>