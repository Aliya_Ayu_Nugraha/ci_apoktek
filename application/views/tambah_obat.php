<div class="col-md-6" style="margin-top: 80px">
<form action="<?=base_url('index.php/obat/proses_obat')?>" method="post">
    <div class="form-group">
      <label >Nama Obat</label>
      <input name="nama_obat" type="text"  class="form-control" placeholder="Nama Obat">
    </div>
    <div class="form-group">
      <label>Harga Obat</label>
      <input name="harga_obat" type="text" class="form-control" placeholder="Harga Obat">
    </div>
    <div class="form-group">
      <label>Pilih Kategori</label>
      <select name="kategori" class="form-control">
        <option></option>
        <?php foreach ($tambah_obat as $kat) {?>
        <option><?=$kat->id_kategori?></option>
        <?php } ?>
      </select>
    </div>
    <input name="tambah_obat" type="submit" class="btn btn-primary" value="Tambah"></input>
</form>
</div>