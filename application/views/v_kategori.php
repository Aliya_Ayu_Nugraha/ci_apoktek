<h2>Kategori</h2>

<div class="container" style="margin-top: 50px">
	<div class="col-md-6">
<table class="table table-striped">
  <tr>

  	<td>No</td>
  	<td>Id Kategori</td>
  	<td>Nama Kategori</td>
  	<td>Aksi</td>
  </tr>

  <?php $no=0;foreach ($tampil_kat as $kat): 
  $no++;?>
  	<tr>
  		<td><?=$no?></td>
  		<td><?=$kat->id_kategori?></td>
  		<td><?=$kat->nama_kategori?></td>

  		<td><a href="<?=base_url('index.php/kategori/tampil_edit_kategori/'.$kat->id_kategori)?>" onclick="edit('<?=$kat->id_kategori?>')" data-toggel="modal" class="btn btn-succes">Ubah</a>
  			<a href="<?=base_url('index.php/kategori/hapus/'.$kat->id_kategori)?>" onclick="return confirm('Apakah anda yakin ?')" class="btn btn-danger">Hapus</a>
  		</td>

  	</tr>
  <?php endforeach ?>
</table>
</div>
</div>